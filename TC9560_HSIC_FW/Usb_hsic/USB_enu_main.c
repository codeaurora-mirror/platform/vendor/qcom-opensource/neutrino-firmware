/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 :  
 */

#include <stdio.h>
#include <stdlib.h>
//#include <USB_enu_main.h>
#include <neu_os.h>
#include <dwc_os.h>
#include <ntn_common.h>
#include "neutrino_defs.h"
#include "dwc_otg_regs.h"
#include "dwc_otg_driver.h"
#include "ntn_uart.h"
#include "ntn_reg_define.h"
#include "net_dev_neu.h"
#include "DWC_ETH_QOS_yregacc.h"
#include "dwc_otg_dbg.h"
#define NTN_HSIC_FW_VER ("01.00.00")
volatile unsigned int malloc_free_counter[4];
volatile unsigned char NetDev_RX_callback[4];

int main()
{
	static int rc = 0;	
	struct lm_device dev;	
	
	/*Neutrino Clock and reset control*/
	REG_WR((volatile uint32_t *)NCLKCTRL,0x00006199);	
	REG_WR((volatile uint32_t *)NRSTCTRL,0x00003FFC);	//reset all peripherals
	REG_WR((volatile uint32_t *)NRSTCTRL,0x00000000);
	

	/* UnMask all Global Interrupts and Select DMA Mode for Data Transfer*/	 
	REG_WR((volatile uint32_t *)GAHBCFG,0x000001A3);	 

	REG_WR(0xE000E404,0x20000000);
  REG_WR(0xE000E408,0x20000000);
  REG_WR(0xE000E40C,0x20202020);
  REG_WR(0xE000E410,0x20202020);
  REG_WR(0xE000E414,0x00002020);

	memset((void *)malloc_free_counter,0,sizeof(volatile unsigned int)*4);
	memset((void *)NetDev_RX_callback,0,sizeof(volatile unsigned char)*4);

	uart_initialize(115200);

	DBG_Info_Print("Welcome to Neutrino HSIC Firmware world!\n");
	DBG_Info_Print("Version: %s\n", NTN_HSIC_FW_VER);

	REG_WR((volatile uint32_t *)NEMACCTL,0x303000A);	
	REG_WR((volatile uint32_t *)NHPLLUPDT,0x3000080);

	/* Enable Interrupts in INTC Block */
	REG_WR((volatile uint32_t *)INTMCUMASK0,0xFFFFFF7F);
	REG_WR((volatile uint32_t *)INTMCUMASK1,0xFFC007FF);
	REG_WR((volatile uint32_t *)INTEXTMASK0,0xFFFFFF7F);
	REG_WR((volatile uint32_t *)INTEXTMASK1,0xFFC007FF);
	REG_WR((volatile uint32_t *)INTINTXMASK0,0xFFFFFF7F);
	REG_WR((volatile uint32_t *)INTINTXMASK1,0xFFC007FF);

	/* Initialize USB Endpoint structure and Enable HSIC endpoint related Interrupt */ 	
	rc = dwc_otg_driver_probe(&dev);
	if(rc != Y_SUCCESS)
		DBG_Error_Print("DWC OTG probe function fails to Initialize Peripheral Controller Driver\n");

	/* Initialize USB Enumeration related Sturcures.*/
	rc = neu_init();
	if(rc != Y_SUCCESS)
		DBG_Error_Print("Neutrino Enumeration Specific Initialization Fails\n");

	/* HSIC Device Control */
	REG_WR(DCTL,0x502);	

	REG_WR(GLPMCFG,0x40000000);		//Core LPM Config
	REG_WR(DCFG,0x8900800);				//Device Configuration
	REG_WR(DIEPMSK,0xffffffff);
	REG_WR(DOEPMSK,0xffffffff);
	REG_WR(DAINTMSK,0xffffffff);

	REG_WR(DPTXFSIZ1,0x01000226);
	REG_WR(DPTXFSIZ2,0x01000326);
	REG_WR(DPTXFSIZ3,0x01000426);
	REG_WR(DPTXFSIZ4,0x01000526);
	REG_WR(DPTXFSIZ5,0x01000626);

	REG_WR(USB_CNTR_STS_REG,0x1d0000);

	/* Enable Cortex M3 NVIC Interrupt for HSIC */
	REG_WR(NVIC_ISER0,(REG_RD(NVIC_ISER0) | (1<<7)));

	REG_WR(DCTL,0x500); 

	/* Initialize eMAC and enable NVIC Interrupt*/
	Ntn_init_network();
	
	while(1)
	{
		#if DBG_EMAC_RX
			for(int i=0;i<4;i++)
			{
				if(malloc_free_counter[i] != 0)
					DBG_eMAC_Print(DBG_EMAC_RX,"Malloc count value ch %d: %d\n", i, malloc_free_counter[i]);
			}
		#endif
		if((malloc_free_counter[0] < IN_REQ_NUMBER - 3)  && NetDev_RX_callback[0])
		{
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH0);
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH1);
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH2);
		}
		else if((malloc_free_counter[1] < IN_REQ_NUMBER - 3)  && NetDev_RX_callback[1])
		{
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH5);
		}
		else if((malloc_free_counter[2] < IN_REQ_NUMBER - 3)  && NetDev_RX_callback[2])
		{
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH4);
		}
		else if((malloc_free_counter[3] < IN_REQ_NUMBER - 3)  && NetDev_RX_callback[3])
		{
			neutrinoINTC_enable_rxtx_interrupt(INTC_Mask_RXCH3);
		}
	}

}
