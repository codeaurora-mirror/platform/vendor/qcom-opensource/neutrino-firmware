/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 : 
 */

/* ==========================================================================
 *
 * Synopsys HS OTG Linux Software Driver and documentation (hereinafter,
 * "Software") is an Unsupported proprietary work of Synopsys, Inc. unless
 * otherwise expressly agreed to in writing between Synopsys and you.
 * 
 * The Software IS NOT an item of Licensed Software or Licensed Product under
 * any End User Software License Agreement or Agreement for Licensed Product
 * with Synopsys or any supplement thereto. You are permitted to use and
 * redistribute this Software in source and binary forms, with or without
 * modification, provided that redistributions of source code must retain this
 * notice. You may not view, use, disclose, copy or distribute this file or
 * any information contained herein except pursuant to this license grant from
 * Synopsys. If you do not agree with this notice, including the disclaimer
 * below, then you are not authorized to use the Software.
 * 
 * THIS SOFTWARE IS BEING DISTRIBUTED BY SYNOPSYS SOLELY ON AN "AS IS" BASIS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE HEREBY DISCLAIMED. IN NO EVENT SHALL SYNOPSYS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * ========================================================================== */

#ifndef __DWC_OTG_DBG_H__
#define __DWC_OTG_DBG_H__

#include "ntn_uart.h"

/** @file
 * This file defines debug levels.
 * Debugging support vanishes in non-debug builds.  
 */

/** When DBG_CIL is define as 1, display CIL Debug messages. */
#define DBG_CIL			0

/** When DBG_CILV is define as 1, display CIL Verbose debug messages */
#define DBG_CILV		0

/**  When DBG_PCD is define as 1, display PCD (Device) debug messages */
#define DBG_PCD			0

/** When DBG_PCDV is define as 1, display PCD (Device) Verbose debug messages */
#define DBG_PCDV		0

/** When DBG_HCD is define as 1, display Host debug messages */
#define DBG_HCD			0

/** When DBG_USB is define as 1, display USB debug messages */
#define DBG_USB			0

/** When DBG_USB is define as 1, display USB debug messages */
#define USB_REG_RD_WR			0
 
/** When DBG_EMAC is define as 1, display eMAC debug messages */
#define DBG_EMAC		0

/** When DBG_INFO is define as 1, display info messages */
#define DBG_INFO		1

/** When DBG_EMAC_RX is define as 1, display eMAC RX debug messages */
#define DBG_EMAC_RX	0

/** When DBG_EMAC_TX is define as 1, display eMAC TX debug messages */
#define DBG_EMAC_TX	0


#define DBG_eMAC_Print(lvl, x...) 			{ if(lvl) NTN_Ser_Printf( x ); }
#define DBG_Info_Print(x...) 			{ if(DBG_INFO) NTN_Ser_Printf( x ); }
#define DBG_Error_Print(x...) 		{ NTN_Ser_Printf("ERROR: "); NTN_Ser_Printf( x ); }
#define DBG_Warn_Print(x...) 			{ NTN_Ser_Printf("WARNING: "); NTN_Ser_Printf( x ); }
#define DBG_USB_Print(lvl, x...)	{ if(lvl) NTN_Ser_Printf( x ); }


#endif
