/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 : 
 */

/* ==========================================================================
 * $File: //dwh/usb_iip/dev/software/otg/linux/drivers/dwc_otg_pcd_linux.c $
 * $Revision: 1.11 $
 * $Date: 2016/07/18 23:50:47 $
 * $Change: 2224063 $
 *
 * Synopsys HS OTG Linux Software Driver and documentation (hereinafter,
 * "Software") is an Unsupported proprietary work of Synopsys, Inc. unless
 * otherwise expressly agreed to in writing between Synopsys and you.
 *
 * The Software IS NOT an item of Licensed Software or Licensed Product under
 * any End User Software License Agreement or Agreement for Licensed Product
 * with Synopsys or any supplement thereto. You are permitted to use and
 * redistribute this Software in source and binary forms, with or without
 * modification, provided that redistributions of source code must retain this
 * notice. You may not view, use, disclose, copy or distribute this file or
 * any information contained herein except pursuant to this license grant from
 * Synopsys. If you do not agree with this notice, including the disclaimer
 * below, then you are not authorized to use the Software.
 *
 * THIS SOFTWARE IS BEING DISTRIBUTED BY SYNOPSYS SOLELY ON AN "AS IS" BASIS
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE HEREBY DISCLAIMED. IN NO EVENT SHALL SYNOPSYS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * ========================================================================== */

/** @file
 * This file implements the Peripheral Controller Driver.
 *
 * The Peripheral Controller Driver (PCD) is responsible for
 * translating requests from the Function Driver into the appropriate
 * actions on the DWC_otg controller. It isolates the Function Driver
 * from the specifics of the controller by providing an API to the
 * Function Driver.
 *
 * The Peripheral Controller Driver for Linux will implement the
 * Gadget API, so that the existing Gadget drivers can be used.
 * (Gadget Driver is the Linux terminology for a Function Driver.)
 *
 * The Linux Gadget API is defined in the header file
 * <code><linux/usb_gadget.h></code>.  The USB EP operations API is
 * defined in the structure <code>usb_ep_ops</code> and the USB
 * Controller API is defined in the structure
 * <code>usb_gadget_ops</code>.
 *
 */

#include "dwc_otg_os_dep.h"
#include "dwc_otg_pcd_if.h"
#include "dwc_otg_pcd.h"
#include "dwc_otg_driver.h"
#include "dwc_otg_dbg.h"
#include "neu_os.h"
#include "ntn_uart.h"
#include "dwc_otg_pcd_linux.h"

#ifndef LM_INTERFACE
#define LM_INTERFACE
#endif

extern cb_handler_t cb_array[2];
extern void (* global_isr_table[])(void);
extern void usb_start_transmit(unsigned char  *p_data, struct usb_request *req, unsigned char bulk_out_num);
void neu_ep_queue(uint32_t buf_addr, const char* ep_name);
unsigned char free_request = 0;
unsigned char *ptr = NULL;
void neu_ep_queue_out(uint32_t req_num, const char* ep_name,uint8_t out_ep);
int neu_ep_queue_in(unsigned char *buf_addr, unsigned char bulk_in_num, unsigned int size);
char debug_ping = 0;


static struct usb_endpoint_descriptor neu_hs_bulk_out1_desc = { 	
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x01,
			/* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() y*/
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05, /* NAK every 1 uframe */
};

static struct usb_endpoint_descriptor neu_hs_bulk_out2_desc = { 	
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x02,
			/* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05, /* NAK every 1 uframe */
};

static struct usb_endpoint_descriptor neu_hs_bulk_out3_desc = { 	
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x03,
			/* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05, /* NAK every 1 uframe */
};


static struct usb_endpoint_descriptor neu_hs_bulk_out4_desc = { 	
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x04,
			/* bEndpointAddress copied from fs_bulk_out_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05, /* NAK every 1 uframe */
};


static struct usb_endpoint_descriptor neu_hs_bulk_in5_desc = { 
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x85,
			/* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05,
};


static struct usb_endpoint_descriptor neu_hs_bulk_in6_desc = { 
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x86,
			/* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05,
};

static struct usb_endpoint_descriptor neu_hs_bulk_in7_desc = { 
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x87,
			/* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05,
};

static struct usb_endpoint_descriptor neu_hs_bulk_in8_desc = { 
			.bLength = USB_DT_ENDPOINT_SIZE, 
			.bDescriptorType = USB_DT_ENDPOINT,
			.bEndpointAddress = 0x88,
			/* bEndpointAddress copied from fs_bulk_in_desc during fsg_bind() */
			.bmAttributes = USB_ENDPOINT_XFER_BULK, 
			.wMaxPacketSize = 512, 
			.bInterval = 0x05,
};

 struct usb_descriptor_header *neu_hs_function[] = {
		(struct usb_descriptor_header *) &neu_intf_desc,
		(struct usb_descriptor_header *) &neu_hs_bulk_in5_desc,
		(struct usb_descriptor_header *) &neu_hs_bulk_in6_desc,	
		(struct usb_descriptor_header *) &neu_hs_bulk_in7_desc,	
		(struct usb_descriptor_header *) &neu_hs_bulk_in8_desc,	
		(struct usb_descriptor_header *) &neu_hs_bulk_out1_desc, 
		(struct usb_descriptor_header *) &neu_hs_bulk_out2_desc,
		(struct usb_descriptor_header *) &neu_hs_bulk_out3_desc,
		(struct usb_descriptor_header *) &neu_hs_bulk_out4_desc, NULL, };



static int neu_setup(struct usb_gadget* gadget, const struct usb_ctrlrequest *setup);

/*** Function Declararions ***/

/* Display the contents of the buffer */
extern void dump_msg(const unsigned char * buf, unsigned int length);
int neu_bind(struct usb_gadget *gadget);
static void neu_unbind(struct usb_gadget *gadget);
static struct usb_request *dwc_otg_pcd_alloc_request(struct usb_ep *ep,
		int gfp_flags);
static void dwc_otg_pcd_free_request(struct usb_ep *ep, struct usb_request *req);

struct neu_dev *the_neu;

static inline int usb_endpoint_maxp(const struct usb_endpoint_descriptor *epd) {
	return epd->wMaxPacketSize;
}




static void bulk_in_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;
	
	bh->inreq_busy = 0;
	bh->state = BUF_STATE_EMPTY;
}

static void bulk_in6_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;
	
	bh->inreq_busy = 0;
	bh->state = BUF_STATE_EMPTY;
}

static void bulk_in7_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;
	
	bh->inreq_busy = 0;
	bh->state = BUF_STATE_EMPTY;
}


static void bulk_in8_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;
	
	bh->inreq_busy = 0;
	bh->state = BUF_STATE_EMPTY;
}


/* Callback function for Legacy traffic */
static void bulk_out0_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;

	//bh->state = BUF_STATE_FULL;
	usb_start_transmit(bh->zoutreq[req->req_num]->buf, req, 0);
}

/* Callback function for AVB Class A traffic */
static void bulk_out1_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;

	usb_start_transmit(bh->zoutreq[req->req_num]->buf, req, 1);
}

/* Callback function for AVB Class B traffic */
static void bulk_out2_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;
	
	usb_start_transmit(bh->zoutreq[req->req_num]->buf, req, 2);
}

/* Callback function for GPTP traffic */
static void bulk_out3_complete(struct usb_ep *ep, struct usb_request *req) {
	struct neu_buffhd *bh = req->context;

	usb_start_transmit(bh->zoutreq[req->req_num]->buf, req, 3);
}



/**
 * Get the dwc_otg_pcd_ep_t* from usb_ep* pointer - NULL in case
 * if the endpoint is not found
 */
 struct dwc_otg_pcd_ep *ep_from_handle(dwc_otg_pcd_t * pcd, void *handle) {
	int i;
	if (pcd->ep0.priv == handle) {
		return &pcd->ep0;
	}

	for (i = 0; i < MAX_EPS_CHANNELS - 1; i++) { // TODO: may have to update the MAX_EPS_CHANNELS for Neutrino
		if (pcd->in_ep[i].priv == handle)
			return &pcd->in_ep[i];
		if (pcd->out_ep[i].priv == handle)
			return &pcd->out_ep[i];
	}

	return NULL;
}

/* USB Endpoint Operations */
/*
 * The following sections briefly describe the behavior of the Gadget
 * API endpoint operations implemented in the DWC_otg driver
 * software. Detailed descriptions of the generic behavior of each of
 * these functions can be found in the Linux header file
 * include/linux/usb_gadget.h.
 *
 * The Gadget API provides wrapper functions for each of the function
 * pointers defined in usb_ep_ops. The Gadget Driver calls the wrapper
 * function, which then calls the underlying PCD function. The
 * following sections are named according to the wrapper
 * functions. Within each section, the corresponding DWC_otg PCD
 * function name is specified.
 *
 */

/**
 * This function is called by the Gadget Driver for each EP to be
 * configured for the current configuration (SET_CONFIGURATION).
 *
 * This function initializes the dwc_otg_ep_t data structure, and then
 * calls dwc_otg_ep_activate.
 */
static int ep_enable(struct usb_ep *usb_ep,
		const struct usb_endpoint_descriptor *ep_desc) {
	int retval;

	DBG_USB_Print(DBG_PCDV, "%s(%p,%p)\n", __func__, usb_ep, ep_desc);

	if (!usb_ep || !ep_desc || ep_desc->bDescriptorType != USB_DT_ENDPOINT) {
		DBG_Warn_Print("%s, bad ep or descriptor\n", __func__);
		return -EINVAL;
	}
	if (usb_ep == &gadget_wrapper->ep0) {
		DBG_Warn_Print("%s, bad ep(0)\n", __func__);
		return -EINVAL;
	}

	/* Check FIFO size? */
	if (!ep_desc->wMaxPacketSize) {
		DBG_Warn_Print("%s, bad %s maxpacket\n", __func__, usb_ep->name);
		return -ERANGE;
	}

	if (!gadget_wrapper->driver
			|| gadget_wrapper->gadget.speed == USB_SPEED_UNKNOWN) {
		DBG_Warn_Print("%s, bogus device state\n", __func__);
		return -ESHUTDOWN;
	}

	retval = dwc_otg_pcd_ep_enable(gadget_wrapper->pcd,
			(const uint8_t *) ep_desc, (void *) usb_ep);
	if (retval) {
		DBG_Warn_Print("dwc_otg_pcd_ep_enable failed\n");
		return -EINVAL;
	}

	usb_ep->maxpacket = ep_desc->wMaxPacketSize; // setting the max packet size used by this ep

	return 0;
}

/**
 * This function is called when an EP is disabled due to disconnect or
 * change in configuration. Any pending requests will terminate with a
 * status of -ESHUTDOWN.
 *
 * This function modifies the dwc_otg_ep_t data structure for this EP,
 * and then calls dwc_otg_ep_deactivate.
 */
static int ep_disable(struct usb_ep *usb_ep) {
	int retval;

	DBG_USB_Print(DBG_PCDV, "%s(%p)\n", __func__, usb_ep);
	if (!usb_ep) {
		DBG_USB_Print(DBG_PCD,
				"%s, %s not enabled\n", __func__, usb_ep ? usb_ep->name : NULL);
		return -EINVAL;
	}

	retval = dwc_otg_pcd_ep_disable(gadget_wrapper->pcd, usb_ep);
	if (retval) {
		retval = -EINVAL;
	}

	return retval;
}

/**
 * This function allocates a request object to use with the specified
 * endpoint.
 *
 * @param ep The endpoint to be used with with the request
 * @param gfp_flags the GFP_* flags to use.
 */
static struct usb_request *dwc_otg_pcd_alloc_request(struct usb_ep *ep,
		int gfp_flags) // TODO: gfp_flags is unused here
{
	struct usb_request *usb_req;

	DBG_USB_Print(DBG_PCDV, "%s(%p,%d)\n", __func__, ep, gfp_flags);
	if (0 == ep) {
		DBG_Warn_Print("%s() %s\n", __func__, "Invalid EP!\n");
		return 0;
	}
	usb_req = DWC_ALLOC(sizeof(struct usb_request));
	
	if (0 == usb_req) {
		DBG_Warn_Print("%s() %s\n", __func__, "request allocation failed!\n");
		return 0;
	}
	memset(usb_req, 0, sizeof(struct usb_request));
	usb_req->dma = DWC_DMA_ADDR_INVALID; // TODO: May have to give the support for this(see usb_ep_alloc_request() in gadget.h)

	return usb_req;
}

/**
 * This function frees a request object.
 *
 * @param ep The endpoint associated with the request
 * @param req The request being freed
 */
static void dwc_otg_pcd_free_request(struct usb_ep *ep, struct usb_request *req) {
	DBG_USB_Print(DBG_PCDV, "%s(%p,%p)\n", __func__, ep, req);

	if (0 == ep || 0 == req) {
		DBG_Warn_Print("%s() %s\n", __func__, "Invalid ep or req argument!\n");
		return;
	}

	DWC_FREE(req);
}

/**
 * This function is used to submit an I/O Request to an EP.
 *
 *	- When the request completes the request's completion callback
 *	  is called to return the request to the driver.
 *	- An EP, except control EPs, may have multiple requests
 *	  pending.
 *	- Once submitted the request cannot be examined or modified.
 *	- Each request is turned into one or more packets.
 *	- A BULK EP can queue any amount of data; the transfer is
 *	  packetized.
 *	- Zero length Packets are specified with the request 'zero'
 *	  flag.
 */
 int ep_queue(struct usb_ep *usb_ep, struct usb_request *usb_req,	gfp_t gfp_flags) {
	dwc_otg_pcd_t *pcd;
	//struct dwc_otg_pcd_ep *ep;
	int retval;
	dma_addr_t dma_addr;

			
	DBG_USB_Print(DBG_PCDV,
			"%s(%p,%p,%d)\n", __func__, usb_ep, usb_req, gfp_flags);

	if (!usb_req || !usb_req->buf) {
		DBG_Warn_Print("bad params\n");
		return -EINVAL;
	}

	if (!usb_ep) {
		DBG_Warn_Print("bad ep\n");
		return -EINVAL;
	}

	pcd = gadget_wrapper->pcd;
	if (!gadget_wrapper->driver
			|| gadget_wrapper->gadget.speed == USB_SPEED_UNKNOWN) {
		DBG_USB_Print(DBG_PCDV,
				"gadget.speed=%d\n", gadget_wrapper->gadget.speed);
		DBG_Warn_Print("bogus device state\n");
		return -ESHUTDOWN;
	}

	DBG_USB_Print(DBG_PCD,
			"%s queue req %p, len %d buf %p\n", usb_ep->name, usb_req, usb_req->length, usb_req->buf);

	usb_req->status = -EINPROGRESS;
	usb_req->actual = 0;

	dma_addr = (dwc_dma_t)usb_req->buf;

	retval = dwc_otg_pcd_ep_queue(pcd, usb_ep, usb_req->buf, dma_addr,
			usb_req->length, usb_req->zero, usb_req);
	if (retval) {
		return -EINVAL;
	}

	return 0;
}

/**
 * This function cancels an I/O request from an EP.
 */
static int ep_dequeue(struct usb_ep *usb_ep, struct usb_request *usb_req) {
	DBG_USB_Print(DBG_PCDV, "%s(%p,%p)\n", __func__, usb_ep, usb_req);

	if (!usb_ep || !usb_req) {
		DBG_Warn_Print("bad argument\n");
		return -EINVAL;
	}
	if (!gadget_wrapper->driver
			|| gadget_wrapper->gadget.speed == USB_SPEED_UNKNOWN) {
		DBG_Warn_Print("bogus device state\n");
		return -ESHUTDOWN;
	}
	if (dwc_otg_pcd_ep_dequeue(gadget_wrapper->pcd, usb_ep, usb_req)) {
		return -EINVAL;
	}

	return 0;
}


/* Function for data travelling from USB to Eth */
void neu_ep_queue_out(uint32_t req_num, const char* ep_name, uint8_t out_ep)
{
	struct neu_buffhd *bh; 
	struct neu_dev *neu = the_neu;

	if(out_ep == 1)
	{	
		bh = &neu->buffhds[4];
		ep_queue(neu->bulk_out1, bh->zoutreq[req_num], 0);
	}
	
	if(out_ep == 2)
	{	
		bh = &neu->buffhds[5];
		ep_queue(neu->bulk_out2, bh->zoutreq[req_num], 0);
	}
	if(out_ep == 3)
	{	
		bh = &neu->buffhds[6];
		ep_queue(neu->bulk_out3, bh->zoutreq[req_num], 0);
	}
	if(out_ep == 4)
	{	
		bh = &neu->buffhds[7];
		ep_queue(neu->bulk_out4, bh->zoutreq[req_num], 0);
	}
	
}


/* Function for data travelling from Neutrino to host */
int neu_ep_queue_in(unsigned char *buf_addr, unsigned char bulk_in_num, unsigned int size)
{
	struct neu_buffhd *bh;
	struct neu_dev *neu = the_neu;
	unsigned char req_num;
	int retval;
	
	bh = &neu->buffhds[bulk_in_num];
	req_num = bh->bi_req_num;
	bh->bi_req_num = (bh->bi_req_num + 1) % IN_REQ_NUMBER;
	bh->buf = buf_addr;
	bh->zinreq[req_num]->buf  = buf_addr;
	bh->zinreq[req_num]->length = size;

	switch (bulk_in_num) {
		case 1:
			retval = ep_queue(neu->bulk_in6, bh->zinreq[req_num], 0);		
		break;
		
		case 2:
			retval = ep_queue(neu->bulk_in7, bh->zinreq[req_num], 0);		
		break;
		
		case 3:
			retval = ep_queue(neu->bulk_in8, bh->zinreq[req_num], 0);		
		break;
		
		case 0:
		default:
			retval = ep_queue(neu->bulk_in5, bh->zinreq[req_num], 0);	
		break;
	}
	
	return retval;
}



/**
 * usb_ep_set_halt stalls an endpoint.
 *
 * usb_ep_clear_halt clears an endpoint halt and resets its data
 * toggle.
 *
 * Both of these functions are implemented with the same underlying
 * function. The behavior depends on the value argument.
 *
 * @param[in] usb_ep the Endpoint to halt or clear halt.
 * @param[in] value
 *	- 0 means clear_halt.
 *	- 1 means set_halt,
 *	- 2 means clear stall lock flag.
 *	- 3 means set  stall lock flag.
 */
static int ep_halt(struct usb_ep *usb_ep, int value) {
	int retval = 0;

	DBG_USB_Print(DBG_PCD, "HALT %s %d\n", usb_ep->name, value);

	if (!usb_ep) {
		DBG_Warn_Print("bad ep\n");
		return -EINVAL;
	}

	retval = dwc_otg_pcd_ep_halt(gadget_wrapper->pcd, usb_ep, value);
	if (retval == -DWC_E_AGAIN) {
		return -EAGAIN;
	} else if (retval) {
		retval = -EINVAL;
	}

	return retval;
}

static int ep_wedge(struct usb_ep *usb_ep) {
	DBG_USB_Print(DBG_PCD, "WEDGE %s\n", usb_ep->name);

	return ep_halt(usb_ep, 3);
}


static struct usb_ep_ops dwc_otg_pcd_ep_ops = { 
	.enable =  ep_enable, 
	.disable = ep_disable,
  .alloc_request = dwc_otg_pcd_alloc_request,
	.free_request =  dwc_otg_pcd_free_request,
  .queue = ep_queue,
	.dequeue = ep_dequeue,
  .set_halt = ep_halt,
  .set_wedge = ep_wedge,
  .fifo_status = 0, 
	.fifo_flush = 0,
	};

/*	Gadget Operations */
/**
 * The following gadget operations will be implemented in the DWC_otg
 * PCD. Functions in the API that are not described below are not
 * implemented.
 *
 * The Gadget API provides wrapper functions for each of the function
 * pointers defined in usb_gadget_ops. The Gadget Driver calls the
 * wrapper function, which then calls the underlying PCD function. The
 * following sections are named according to the wrapper functions
 * (except for ioctl, which doesn't have a wrapper function). Within
 * each section, the corresponding DWC_otg PCD function name is
 * specified.
 *
 */

/**
 *Gets the USB Frame number of the last SOF.
 */
static int get_frame_number(struct usb_gadget *gadget) {
	struct gadget_wrapper *d;

	DBG_USB_Print(DBG_PCDV, "%s(%p)\n", __func__, gadget);

	if (gadget == 0) {
		return -ENODEV;
	}
	return dwc_otg_pcd_get_frame_number(d->pcd);
}

#ifdef CONFIG_USB_DWC_OTG_LPM
static int test_lpm_enabled(struct usb_gadget *gadget)
{
	struct gadget_wrapper *d;

	d = container_of(gadget, struct gadget_wrapper, gadget);

	return dwc_otg_pcd_is_lpm_enabled(d->pcd);
}
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0)
static int test_besl_enabled(struct usb_gadget *gadget)
{
	struct gadget_wrapper *d;

	d = container_of(gadget, struct gadget_wrapper, gadget);

	return dwc_otg_pcd_is_besl_enabled(d->pcd);
}

static int get_param_baseline_besl(struct usb_gadget *gadget)
{
	struct gadget_wrapper *d;

	d = container_of(gadget, struct gadget_wrapper, gadget);

	return dwc_otg_pcd_get_param_baseline_besl(d->pcd);
}


static int get_param_deep_besl(struct usb_gadget *gadget)
{
	struct gadget_wrapper *d;

	d = container_of(gadget, struct gadget_wrapper, gadget);

	return dwc_otg_pcd_get_param_deep_besl(d->pcd);
}
#endif
#endif

/**
 * Initiates Session Request Protocol (SRP) to wakeup the host if no
 * session is in progress. If a session is already in progress, but
 * the device is suspended, remote wakeup signaling is started.
 *
 */
static int wakeup(struct usb_gadget *gadget) {
	struct gadget_wrapper *d;

	DBG_USB_Print(DBG_PCDV, "%s(%p)\n", __func__, gadget);

	if (gadget == 0) {
		return -ENODEV;
	} 
	dwc_otg_pcd_wakeup(d->pcd);
	return 0;
}

static struct usb_gadget_ops dwc_otg_pcd_ops = {

  .get_frame = get_frame_number,
	.wakeup = wakeup,
#ifdef CONFIG_USB_DWC_OTG_LPM
	.lpm_support = test_lpm_enabled,
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,6,0)	
	.besl_support = test_besl_enabled,
	.get_baseline_besl = get_param_baseline_besl,
	.get_deep_besl = get_param_deep_besl,
#endif	
#endif
		};

static int _setup(dwc_otg_pcd_t * pcd, uint8_t * bytes) {
	
	int retval = -DWC_E_NOT_SUPPORTED;
	if (gadget_wrapper->driver && gadget_wrapper->driver->setup) {
		neu_setup(&gadget_wrapper->gadget, (struct usb_ctrlrequest*) bytes);
	}
	retval = 1;
	if (retval == -ENOTSUPP) {
		retval = -DWC_E_NOT_SUPPORTED;
	} else if (retval < 0) {
		retval = -DWC_E_INVALID;
	}

	return retval;
}


static int _complete(dwc_otg_pcd_t *pcd, void *ep_handle, void *req_handle,
		int32_t status, uint32_t actual) {
	  struct usb_request *req = (struct usb_request *) req_handle;

		if (req && req->complete) {
		switch (status) {
		case -DWC_E_SHUTDOWN:
			req->status = -ESHUTDOWN;
			break;
		case -DWC_E_RESTART:
			req->status = -ECONNRESET;
			break;
		case -DWC_E_INVALID:
			req->status = -EINVAL;
			break;
		case -DWC_E_TIMEOUT:
			req->status = -ETIMEDOUT;
			break;
		default:
			req->status = status;

		}

		req->actual = actual;
		req->complete(ep_handle, req);
	}

	return 0;
}

static int _connect(dwc_otg_pcd_t * pcd, int speed) {
	gadget_wrapper->gadget.speed = (enum usb_device_speed)speed;
	return 0;
}

static int _disconnect(dwc_otg_pcd_t * pcd) {
	if (gadget_wrapper->driver && gadget_wrapper->driver->disconnect) {
		gadget_wrapper->driver->disconnect(&gadget_wrapper->gadget);
	}
	return 0;
}

static int _resume(dwc_otg_pcd_t * pcd) {
	if (gadget_wrapper->driver && gadget_wrapper->driver->resume) {
		gadget_wrapper->driver->resume(&gadget_wrapper->gadget);
	}

	return 0;
}

static int _suspend(dwc_otg_pcd_t * pcd) {
	if (gadget_wrapper->driver && gadget_wrapper->driver->suspend) {
		gadget_wrapper->driver->suspend(&gadget_wrapper->gadget);
	}
	return 0;
}

/**
 * This function updates the otg values in the gadget structure.
 */
static int _hnp_changed(dwc_otg_pcd_t * pcd) {

	if (!gadget_wrapper->gadget.is_otg)
		return 0;

	gadget_wrapper->gadget.b_hnp_enable = get_b_hnp_enable(pcd);
	gadget_wrapper->gadget.a_hnp_support = get_a_hnp_support(pcd);
	gadget_wrapper->gadget.a_alt_hnp_support = get_a_alt_hnp_support(pcd);
	return 0;
}

static int _reset(dwc_otg_pcd_t * pcd) {
	return 0;
}

static const struct dwc_otg_pcd_function_ops fops = {

    .complete = _complete,
		.setup = _setup, 
	  .disconnect = _disconnect,
   	.connect = _connect,
		.resume = _resume,
   	.suspend = _suspend, 
	  .hnp_changed = _hnp_changed,
		.reset = _reset,

		};

/**
 * This function is the top level PCD interrupt handler.
 */
void dwc_otg_pcd_irq(void *dev) {
	dwc_otg_pcd_t *pcd = dev;
	int32_t retval = IRQ_NONE;

	retval = dwc_otg_pcd_handle_intr(pcd);
	if (retval != 0) {
		S3C2410X_CLEAR_EINTPEND();
	}

}

/**
 * This function initialized the usb_ep structures to their default
 * state.
 *
 * @param d Pointer on gadget_wrapper.
 */
void gadget_add_eps(struct gadget_wrapper *d) {
	static const char *names[] = {

	"ep0", "ep1in", "ep2in", "ep3in", "ep4in", "ep5in", "ep6in", "ep7in",
			"ep8in", "ep9in", "ep10in", "ep11in", "ep12in", "ep13in", "ep14in",
			"ep15in", "ep1out", "ep2out", "ep3out", "ep4out", "ep5out",
			"ep6out", "ep7out", "ep8out", "ep9out", "ep10out", "ep11out",
			"ep12out", "ep13out", "ep14out", "ep15out" };

	int i;
	struct usb_ep *ep;
	signed char dev_endpoints;

	d->gadget.ep0 = &d->ep0;
	d->gadget.speed = USB_SPEED_UNKNOWN;
	d->gadget.max_speed = USB_SPEED_HIGH;

	/**
	 * Initialize the EP0 structure.
	 */
	ep = &d->ep0;

	/* Init the usb_ep structure. */
	ep->name = names[0];
	ep->ops = (struct usb_ep_ops *) &dwc_otg_pcd_ep_ops;

	/**
	 * @todo NGS: Set the max packet size for EP
	 */
	ep->maxpacket = MAX_PACKET_SIZE;
	dwc_otg_pcd_ep_enable(d->pcd, NULL, ep);

	/**
	 * Initialize the EP structures.
	 */
	dev_endpoints = d->pcd->core_if->dev_if->num_in_eps;

	for (i = 0; i < dev_endpoints; i++) {
		ep = &d->in_ep[i];

		/* Init the usb_ep structure. */
		ep->name = names[d->pcd->in_ep[i].dwc_ep.num];
		ep->ops = (struct usb_ep_ops *) &dwc_otg_pcd_ep_ops;

		/**
		 * Set the max packet size for the EP
		 */
		ep->maxpacket = MAX_PACKET_SIZE;
	}

	dev_endpoints = d->pcd->core_if->dev_if->num_out_eps;

	for (i = 0; i < dev_endpoints; i++) {
		ep = &d->out_ep[i];

		/* Init the usb_ep structure. */
		ep->name = names[15 + d->pcd->out_ep[i].dwc_ep.num];
		ep->ops = (struct usb_ep_ops *) &dwc_otg_pcd_ep_ops;

		/**
		 * Set the max packet size for the EP
		 */
		ep->maxpacket = MAX_PACKET_SIZE;

	}

	/* Allocate Packet size for EP0. */
	d->ep0.maxpacket = MAX_EP0_SIZE;
}

/**
 * This function allocated memory to gadget wrapper structure.
 * state.
 *
 */
static struct gadget_wrapper *alloc_wrapper(struct lm_device *_dev)
{
	static char pcd_name[] = "dwc_otg_pcd";
	dwc_otg_device_t *otg_dev = lm_get_drvdata(_dev);

	struct gadget_wrapper *d;

	d = DWC_ALLOC(sizeof(*d));
	if (d == NULL) {
		return NULL;
	}

	memset(d, 0, sizeof(*d));

	d->gadget.name = pcd_name;
	d->pcd = otg_dev->pcd;

	d->gadget.ops = &dwc_otg_pcd_ops;

	return d;
}

/**
 * This function initialized the PCD portion of the driver.
 *
 */
int pcd_init(struct lm_device *_dev) 
{
	dwc_otg_device_t *otg_dev = lm_get_drvdata(_dev);
	int retval = 0;

	otg_dev->pcd = dwc_otg_pcd_init(otg_dev->core_if);

	if (!otg_dev->pcd) {
		return -ENOMEM;
	}

	otg_dev->pcd->otg_dev = otg_dev;

	/* Memory allocation to endpoing structure */
	gadget_wrapper = alloc_wrapper(_dev);

	/*
	 * Initialize EP structures
	 */
	gadget_add_eps(gadget_wrapper);
	/*
	 * Setup interupt handler
	 */
	cb_array[1].handler = dwc_otg_pcd_irq;
	cb_array[1].dev = otg_dev->pcd;

	dwc_otg_pcd_start(gadget_wrapper->pcd, &fops);

	return retval;
}


/**
 * Cleanup the PCD.
 */
void pcd_remove(struct lm_device *_dev)
{
	dwc_otg_device_t *otg_dev = lm_get_drvdata(_dev);
}

/**
 * This function registers a gadget driver with the PCD.
 *
 * When a driver is successfully registered, it will receive control
 * requests including set_configuration(), which enables non-control
 * requests.  then usb traffic follows until a disconnect is reported.
 * then a host may connect again, or the driver might get unbound.
 *
 * @param driver The driver being registered
 * @param bind The bind function of gadget driver
 */
int usb_gadget_probe_driver(struct usb_gadget_driver *driver,
		int (*bind)(struct usb_gadget *)) {
	int retval;

	if (gadget_wrapper == 0) {
		DBG_USB_Print(DBG_PCDV, "ENODEV\n");
		return -ENODEV;
	}
	if (gadget_wrapper->driver != 0) {
		DBG_USB_Print(DBG_PCDV, "EBUSY (%p)\n", gadget_wrapper->driver);
		return -EBUSY;
	}
	/* hook up the driver */
	gadget_wrapper->driver = driver;
	retval = bind(&gadget_wrapper->gadget);

	if (retval) {
		gadget_wrapper->driver = 0;
		gadget_wrapper->gadget.dev.driver = 0;
		return retval;
	}
	return 0;
}

/**
 * This function unregisters a gadget driver
 *
 * @param driver The driver being unregistered
 */
int usb_gadget_unregister_driver(struct usb_gadget_driver *driver) {

	if (gadget_wrapper == 0) {
		DBG_USB_Print(DBG_USB, "%s Return(%d): s_pcd==0\n", __func__, -ENODEV);
		return -ENODEV;
	}
	if (driver == 0 || driver != gadget_wrapper->driver) {
		DBG_USB_Print(DBG_USB, "%s Return(%d): driver?\n", __func__, -EINVAL);
		return -EINVAL;
	}

	driver->unbind(&gadget_wrapper->gadget);
	gadget_wrapper->driver = 0;

	return 0;
}

struct usb_gadget_driver neu_driver = { 
		.max_speed = USB_SPEED_HIGH,
		.bind = neu_bind, 	   
		.unbind = neu_unbind,
		.setup = neu_setup, // neu_setup should be called from gadget driver setup()
		};


int do_set_interface(struct neu_dev *neu, int altsetting) {	
	int rc = 0;
	int i;
	const struct usb_endpoint_descriptor *d;
	struct neu_buffhd *bh; 

	if (neu->running)
		DBG_USB_Print(DBG_USB,"reset interface\n");

	
	reset:
	/* Disable the endpoints */
	if (neu->bulk_in5_enabled) {
		ep_disable(neu->bulk_in5);
		neu->bulk_in5_enabled = 0;
	}
	if (neu->bulk_in6_enabled) {
		ep_disable(neu->bulk_in6);
		neu->bulk_in6_enabled = 0;
	}

	if (neu->bulk_in7_enabled) {
		ep_disable(neu->bulk_in7);
		neu->bulk_in7_enabled = 0;
	}

	if (neu->bulk_in8_enabled) {
		ep_disable(neu->bulk_in8);
		neu->bulk_in8_enabled = 0;
	}
	
	if (neu->bulk_out1_enabled) {
		ep_disable(neu->bulk_out1);
		neu->bulk_out1_enabled = 0;
	}

	if (neu->bulk_out2_enabled) {
		ep_disable(neu->bulk_out2);
		neu->bulk_out2_enabled = 0;
	}

	if (neu->bulk_out3_enabled) {
		ep_disable(neu->bulk_out3);
		neu->bulk_out3_enabled = 0;
	}

	if (neu->bulk_out4_enabled) {
		ep_disable(neu->bulk_out4);
		neu->bulk_out4_enabled = 0;
	}
	
	neu->running = 0;
	if (altsetting < 0 || rc != 0)
		return rc;

	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_in5_desc;
	if ((rc = ep_enable(neu->bulk_in5, d)) != 0)
		goto reset;
	neu->bulk_in5_enabled = 1;

	
	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_in6_desc;
	if ((rc = ep_enable(neu->bulk_in6, d)) != 0)
		goto reset;
	neu->bulk_in6_enabled = 1;

	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_in7_desc;
	if ((rc = ep_enable(neu->bulk_in7, d)) != 0)
		goto reset;
	neu->bulk_in7_enabled = 1;

	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_in8_desc;
	if ((rc = ep_enable(neu->bulk_in8, d)) != 0)
		goto reset;
	neu->bulk_in8_enabled = 1;
	
	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_out1_desc;
	if ((rc = ep_enable(neu->bulk_out1, d)) != 0)
		goto reset;
	neu->bulk_out1_enabled = 1;

	
	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_out2_desc;
	if ((rc = ep_enable(neu->bulk_out2, d)) != 0)
		goto reset;
	neu->bulk_out2_enabled = 1;

	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_out3_desc;
	if ((rc = ep_enable(neu->bulk_out3, d)) != 0)
		goto reset;
	neu->bulk_out3_enabled = 1;

	if (neu->gadget->speed == USB_SPEED_HIGH)
		d = &neu_hs_bulk_out4_desc;
	if ((rc = ep_enable(neu->bulk_out4, d)) != 0)
		goto reset;
	neu->bulk_out4_enabled = 1;
	
	neu->bulk_out_maxpacket = usb_endpoint_maxp(d);

	bh = &neu->buffhds[0];
	bh->bi_req_num = 0;
	for (i=0; i < IN_REQ_NUMBER; i++) 
	{
		bh->zinreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_in5, GFP_KERNEL);
		bh->zinreq[i]->req_num = i;
		bh->zinreq[i]->buf  = bh->buf;
		bh->zinreq[i]->context = bh;
		neu->running = 1;
		bh->zinreq[i]->complete = bulk_in_complete;
		bh->zinreq[i]->length = BH_BUF_SIZE;
		DBG_USB_Print(DBG_USB,"zz: in1 req=%p\n",i, bh->zinreq[i]);
	}
	
	bh = &neu->buffhds[1];
	bh->bi_req_num = 0;
	for (i=0; i < IN_REQ_NUMBER; i++) 
	{
		bh->zinreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_in6, GFP_KERNEL);
		bh->zinreq[i]->req_num = i;
		bh->zinreq[i]->buf  = bh->buf;
		bh->zinreq[i]->context = bh;
		neu->running = 1;
		bh->zinreq[i]->complete = bulk_in6_complete;
		bh->zinreq[i]->length = BH_BUF_SIZE;
		DBG_USB_Print(DBG_USB,"zz: in2 req=%p\n",i, bh->zinreq[i]);
	}
	
	bh = &neu->buffhds[2];
	bh->bi_req_num = 0;
	for (i=0; i < IN_REQ_NUMBER; i++) 
	{
		bh->zinreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_in7, GFP_KERNEL);
		bh->zinreq[i]->req_num = i;
		bh->zinreq[i]->buf  = bh->buf;
		bh->zinreq[i]->context = bh;
		neu->running = 1;
		bh->zinreq[i]->complete = bulk_in7_complete;
		bh->zinreq[i]->length = BH_BUF_SIZE;
		DBG_USB_Print(DBG_USB,"zz: in3 req=%p\n",i, bh->zinreq[i]);
	}
	

	bh = &neu->buffhds[3];  //GPTP
	bh->bi_req_num = 0;
	for (i=0; i < IN_REQ_NUMBER; i++) 
	{
		bh->zinreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_in8, GFP_KERNEL);
		bh->zinreq[i]->req_num = i;
		//bh->inreq->buf  = bh->buf;		
		bh->zinreq[i]->context = bh;
		neu->running = 1;
		bh->zinreq[i]->complete = bulk_in8_complete;
		bh->zinreq[i]->length = BH_BUF_PTP_SIZE;
		DBG_USB_Print(DBG_USB,"zz: in4 req=%p\n",i, bh->zinreq[i]);
	}

	bh = &neu->buffhds[4];
	for (i=0; i < OUT_REQ_NUMBER; i++) 
	{
		bh->buf = DWC_ALLOC(BH_BUF_SIZE); 
		
		bh->zoutreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_out1, GFP_KERNEL);
		bh->zoutreq[i]->context = bh;
		bh->zoutreq[i]->complete = bulk_out0_complete;
		bh->zoutreq[i]->length = BH_BUF_SIZE;
		bh->zoutreq[i]->req_num = i;
		bh->zoutreq[i]->buf  = bh->buf;
		
		DBG_USB_Print(DBG_USB,"zz: out1 buf%d=%p\n",i, bh->buf);
		ep_queue(neu->bulk_out1, bh->zoutreq[i], 0);
	}
	neu->running = 1;

	bh = &neu->buffhds[5];
	for (i=0; i < OUT_REQ_NUMBER; i++) 
	{
		bh->buf = DWC_ALLOC(BH_BUF_SIZE); 
		
		bh->zoutreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_out2, GFP_KERNEL);
		bh->zoutreq[i]->context = bh;
		bh->zoutreq[i]->complete = bulk_out1_complete;
		bh->zoutreq[i]->length = BH_BUF_SIZE;
		bh->zoutreq[i]->req_num = i;
		bh->zoutreq[i]->buf  = bh->buf;
		
		DBG_USB_Print(DBG_USB,"zz: out2 buf%d=%p\n",i, bh->buf);
		ep_queue(neu->bulk_out2, bh->zoutreq[i], 0);
	}
	neu->running = 1;
	

	bh = &neu->buffhds[6];
	for (i=0; i < OUT_REQ_NUMBER; i++) 
	{
		bh->buf = DWC_ALLOC(BH_BUF_SIZE); 
		
		bh->zoutreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_out3, GFP_KERNEL);
		bh->zoutreq[i]->context = bh;
		bh->zoutreq[i]->complete = bulk_out2_complete;
		bh->zoutreq[i]->length = BH_BUF_SIZE;
		bh->zoutreq[i]->req_num = i;
		bh->zoutreq[i]->buf  = bh->buf;
		
		DBG_USB_Print(DBG_USB,"zz: out3 buf%d=%p\n",i, bh->buf);
		ep_queue(neu->bulk_out3, bh->zoutreq[i], 0);
	}
	neu->running = 1;

	bh = &neu->buffhds[7];
	for (i=0; i < OUT_REQ_NUMBER; i++) 
	{
		bh->buf = DWC_ALLOC(BH_BUF_PTP_SIZE); 
		
		bh->zoutreq[i] = dwc_otg_pcd_alloc_request(neu->bulk_out4, GFP_KERNEL);
		bh->zoutreq[i]->context = bh;
		bh->zoutreq[i]->complete = bulk_out3_complete;
		bh->zoutreq[i]->length = BH_BUF_PTP_SIZE;
		bh->zoutreq[i]->req_num = i;
		bh->zoutreq[i]->buf  = bh->buf;
		
		DBG_USB_Print(DBG_USB,"zz: out4 buf%d=%p\n",i, bh->buf);
		ep_queue(neu->bulk_out4, bh->zoutreq[i], 0);
	}
	neu->running = 1;
	
	return rc;
}




static int do_set_config(struct neu_dev *neu, uint8_t new_config) {
	int rc = 0;

	rc = do_set_interface(neu, 0);
	return rc;
}


static int neu_setup(struct usb_gadget* gadget, const struct usb_ctrlrequest *ctrl) {
	struct usb_request *req = the_neu->ep0req; // TODO: how to get the ref struct usb_request
		
	struct neu_dev *neu = (struct neu_dev *) dev_get_drvdata(&gadget->dev);

	req->length = 512;
	do_set_config(neu, neu->new_config);
	
	return 0;
}



static void neu_unbind(struct usb_gadget *gadget) {
	struct neu_dev *neu = (struct neu_dev *) dev_get_drvdata(&gadget->dev);
	int i, j;

	struct usb_request *req = neu->ep0req;

	DBG_USB_Print(DBG_USB,"unbind\n");

	/* If the thread isn't already dead, tell it to exit now */
	if (neu->state != NEU_STATE_TERMINATED) {
		do_set_config(neu, 0);
		neu->state = NEU_STATE_TERMINATED;
	}

	/* Free the data buffers */
	for (i = 4; i < 8; i++) {
		for (j = 0; j < OUT_REQ_NUMBER; j++) {
			DWC_FREE(neu->buffhds[i].zoutreq[j]->buf);
		}
	}

	/* Free the request and buffer for endpoint 0 */
	if (req) {
		DWC_FREE(req->buf);
		dwc_otg_pcd_free_request(neu->ep0, req);
	}

	dev_set_drvdata(&gadget->dev, NULL);
}


int neu_bind(struct usb_gadget *gadget) {
	
	struct neu_dev *neu = the_neu;
	int  rc;
	struct usb_request *req;
	struct usb_ep *ep;

	neu->gadget = gadget;
	dev_set_drvdata(&gadget->dev, neu);
	neu->ep0 = gadget->ep0;
	neu->ep0->driver_data = neu;

	ep = &gadget_wrapper->in_ep[0];  
	if(!ep)
	   goto autoconf_fail;
	
	ep->driver_data = neu; // claim the endpoint
	neu->bulk_in5 = ep;

	ep = &gadget_wrapper->in_ep[1]; 
	if(!ep)
	   goto autoconf_fail;
	
	ep->driver_data = neu; // claim the endpoint
	neu->bulk_in6 = ep;

	ep = &gadget_wrapper->in_ep[2];  
	if(!ep)
	   goto autoconf_fail;

	
	ep->driver_data = neu; // claim the endpoint
	neu->bulk_in7 = ep;

	ep = &gadget_wrapper->in_ep[3];  
	if(!ep)
	   goto autoconf_fail;
	
	ep->driver_data = neu; // claim the endpoint
	neu->bulk_in8 = ep;

	ep = &gadget_wrapper->out_ep[0]; 
	if(!ep)
		goto autoconf_fail;


	ep->driver_data = neu; // claim the endpoint
	neu->bulk_out1 = ep;

	ep = &gadget_wrapper->out_ep[1]; 
	if(!ep)
		goto autoconf_fail;

	ep->driver_data = neu; // claim the endpoint
	neu->bulk_out2 = ep;

	ep = &gadget_wrapper->out_ep[2]; 
	if(!ep)
		goto autoconf_fail;

	ep->driver_data = neu; // claim the endpoint
	neu->bulk_out3 = ep;

	ep = &gadget_wrapper->out_ep[3]; 
	if(!ep)
		goto autoconf_fail;

	ep->driver_data = neu; // claim the endpoint
	neu->bulk_out4 = ep;


	/* Allocate the request and buffer for endpoint 0 */
	neu->ep0req = req = dwc_otg_pcd_alloc_request(neu->ep0, GFP_KERNEL); 
	if (!req)
		goto out;
	req->buf = DWC_ALLOC(EP0_BUFSIZE);
	if (!req->buf)
		goto out;
	

	return 0;

	autoconf_fail:
	DBG_Error_Print("unable to autoconfigure all endpoints\n");
	rc = -ENOTSUPP;

	out: neu->state = NEU_STATE_TERMINATED; 
	neu_unbind(gadget);
	return rc;
	
}

int neu_init(void) {

	int rc;

	struct neu_dev *neu = NULL;

	/*Allocate neu device structure*/
	rc = sizeof *neu;
	neu = DWC_ALLOC(rc);
	if (!neu)
		return -ENOMEM;

	the_neu = neu;
	rc = usb_gadget_probe_driver(&neu_driver, neu_driver.bind);
	if (rc != 0)
		DBG_Error_Print("usb_gadget_probe_driver EINVAL\n");

	return rc;	
}


