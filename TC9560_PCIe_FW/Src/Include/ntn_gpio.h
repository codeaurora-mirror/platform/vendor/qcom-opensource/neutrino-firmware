#ifndef __GPIO_H__
#define __GPIO_H__
/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */
 
#define  NTN_GPIO_REG_BASE            (unsigned int)(0x40001200) 
#define  NTN_GPIO_INPUT0              (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0000))
#define  NTN_GPIO_INPUT1              (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0004))
#define  NTN_GPIO_INPUT_ENABLE0       (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0008)) /* 0-31 */
#define  NTN_GPIO_INPUT_ENABLE1       (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x000C)) /* 32-63 */
#define  NTN_GPIO_OUTPUT0             (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0010))
#define  NTN_GPIO_OUTPUT1             (*(unsigned int *)(NTN_GPIO_REG_BASE + 0x0014))
		
	

void taec_gpio0_config_output( unsigned int data);
void taec_gpio0_output_data(unsigned int data);

void taec_gpio1_config_output( unsigned int data);
void taec_gpio1_output_data(unsigned int data);

#endif 

